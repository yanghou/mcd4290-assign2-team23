/*
 * @Author       : DWX
 * @Date         : 2020-09-07 19:45:34
 * @LastEditors  : DWX
 * @LastEditTime : 2020-09-07 20:37:17
 * @Description  : 
 */
var tbody = document.getElementById("portList");

var ports = JSON.parse(localStorage.getItem("portList"))
var tbHtml = "";

for (var i = 0; i < ports.length; i++) {
    if (ports[i].portName === '') continue;
    var trHtml = getPortHtml(ports[i]);

    tbHtml = tbHtml + "<tr>" + trHtml + "</tr>"
}
tbody.innerHTML = tbHtml;


function getPortHtml(port) {
    var tr = "<td>" + port.portName + "</td>";
    tr = tr + "<td>" + port.inCountry + "</td>";
    tr = tr + "<td>" + port.portType + "</td>";
    tr = tr + "<td>" + port.portSize + "</td>";
    tr = tr + "<td>" + port.portLocation + "</td>";
    return tr;
}
