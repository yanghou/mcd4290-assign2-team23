let url = "https://eng1003.monash/api/v1/ships/?callback=shipAPI"
let script = document.createElement('script');
script.src = url;
document.body.appendChild(script);
function shipAPI(shipList){
    let viewShipsTableRef = document.getElementById("ViewShip")
    
    for (let i = 0; i < shipList.ships.length ; i++ ){
        
       
        let name = shipList.ships[i].name
        let maxSpeed = shipList.ships[i].maxSpeed
        let range = shipList.ships[i].range
        let description = shipList.ships[i].desc
        let cost = shipList.ships[i].cost
        let output = ""
        output += "<tr class='tableRowStyle0'>"
        output += "<td class='mdl-data-table__cell--non-numeric'>" + name +  "</td>"
        output += "<td>" + maxSpeed + "</td>"
        output += "<td>" + range + "</td>"
        output += "<td class='mdl-data-table__cell--non-numeric '>" + description + "</td>"
        output += "<td>" + cost + "</td>"
        output +="</tr>"
        viewShipsTableRef.innerHTML += output
    }
}



let shipList = JSON.parse(localStorage.getItem("ships"))
console.log(shipList)//check the shiplist
let viewShipsTableRef = document.getElementById("ViewShip")
for (let i = 0; i < shipList.length ; i++ ){
    
    let name = shipList.ships[i]._name
    let maxSpeed = shipList.ships[i]._maxSpeed
    let range = shipList.ships[i]._range
    let description = shipList.ships[i]._description
    let cost = shipList.ships[i]._cost
    let output = ""
    output += "<tr class='tableRowStyle0'>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + name +  "</td>"
    output += "<td>" + maxSpeed + "</td>"
    output += "<td>" + range + "</td>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + description + "</td>"
    output += "<td>" + cost + "</td>"
    output +="</tr>"
    viewShipsTableRef.innerHTML += output
}