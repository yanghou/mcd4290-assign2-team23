'use strict';

getDataByUrl("https://eng1003.monash/api/v1/ports/");

var fromLon, fromLat, toLon, toLat,cLng, cLat
var historys = [];




function getDataByUrl(url) {
    var xhr = new XMLHttpRequest();
    var uri = url;
    xhr.open("Get", uri, true);
    xhr.setRequestHeader("Accept", "application/json;charset=UTF-8");
    xhr.onload = () => {
        var datas = JSON.parse(xhr.responseText);
        console.log(datas);
        var obj = {};
        var countrys = [];


        for (var i = 0; i < datas.ports.length; i++) {
            if (countrys.indexOf(datas.ports[i].country) == -1) {
                var str = '<option value="' + datas.ports[i].country + '">' + datas.ports[i].country + '</option>';
                obj[datas.ports[i].country] = [datas.ports[i].name];
                document.getElementById("scountry").innerHTML += str;
                document.getElementById("dcountry").innerHTML += str;
                countrys.push(datas.ports[i].country);
            } else {

                if (obj[datas.ports[i].country].indexOf(datas.ports[i].name) == -1) {
                    obj[datas.ports[i].country].push(datas.ports[i].name);
                }
            }
        }

        document.getElementById("scountry").onchange = function () {
            var thisCountry = document.getElementById("scountry").value;
            var ports = obj[thisCountry];
            var str = '';
            for (var i = 0; i < ports.length; i++) {
                str += '<option value="' + ports[i] + '">' + ports[i] + '</option>';
            }
            document.getElementById("sport").innerHTML = str;
        };


        document.getElementById("dcountry").onchange = function () {
            var thisCountry = document.getElementById("dcountry").value;
            var ports = obj[thisCountry];
            var str = '';
            for (var i = 0; i < ports.length; i++) {
                str += '<option value="' + ports[i] + '">' + ports[i] + '</option>';
            }
            document.getElementById("dport").innerHTML = str;
        };



        document.getElementById("sport").onchange = function () {
            var thisPort = datas.ports.filter(o => o.name == this.value)[0];
            history.fromPort = thisPort;
            fromLon = thisPort.lng;
            fromLat = thisPort.lat;
            var marker1 = new mapboxgl.Marker({})
                .setLngLat([fromLon, fromLat])
                .addTo(map);
        };

        document.getElementById("dport").onchange = function () {
            var thisPort = datas.ports.filter(o => o.name == this.value)[0];
            history.toPort = thisPort;
            toLon = thisPort.lng;
            toLat = thisPort.lat;
            var marker2 = new mapboxgl.Marker({})
                .setLngLat([toLon, toLat])
                .addTo(map);
           

            var from = turf.point([fromLon, fromLat]);
            var to = turf.point([toLon, toLat]);
            var options = { units: 'miles' };
            var distance = turf.distance(from, to, options);
            document.getElementById("distance").innerHTML = distance.toFixed(2);
            console.log(distance);

            var xhrShip = new XMLHttpRequest();
            xhrShip.open("Get", "https://eng1003.monash/api/v1/ships/", true);
            xhrShip.setRequestHeader("Accept", "application/json;charset=UTF-8");
            xhrShip.onload = () => {
                var ships = JSON.parse(xhrShip.responseText);
                document.getElementById("ship").innerHTML = "";
                for (var i = 0; i < ships.ships.length; i++) {
                    if (ships.ships[i].range > distance) {
                        document.getElementById("ship").innerHTML += "<option value='" + ships.ships[i].name + "'>" + ships.ships[i].name + "</option>";
                    }
                }
         
                document.getElementById("ship").onchange = function () {
                    var shipName = this.value;
                    var spend = ships.ships.filter(o => o.name == shipName)[0];
                    document.getElementById("cost").innerHTML = (spend.cost * distance).toFixed(2);
                    document.getElementById("time").innerHTML = (distance / spend.maxSpeed).toFixed(2);
                    history.shipName = shipName;
                    history.cost = (spend.cost * distance).toFixed(2);
                    history.time = (distance / spend.maxSpeed).toFixed(2);
                }
            };
            xhrShip.send(null);


        };


        mapboxgl.accessToken = 'pk.eyJ1IjoieWFuZzEyMzEyMyIsImEiOiJja2VwYTg1ZmUwZmhsMnpwZHZkNTJtZXQxIn0.oDlXoTGhv4ewypKpHYR3fw';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11'
        });
        mapboxgl.accessToken = 'pk.eyJ1IjoieWFuZzEyMzEyMyIsImEiOiJja2VwYTg1ZmUwZmhsMnpwZHZkNTJtZXQxIn0.oDlXoTGhv4ewypKpHYR3fw';
        var coordinates = document.getElementById('coordinates');
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [0, 0],
            zoom: 2
        });
       



    }
    xhr.send(null);

}






function sub() {
    if (localStorage.historys == undefined) {
        historys=[history];
        localStorage.historys = JSON.stringify(historys);
    } else {
        historys = JSON.parse(localStorage.historys);
        historys.push(history);
        localStorage.historys = JSON.stringify(historys);
    }
    alert("Save Successfully");
}



//if (localStorage.historys != undefined) {
//    var hs = JSON.parse(localStorage.historys);
//}







